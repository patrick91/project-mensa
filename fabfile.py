from fabric.api import *
from fabric.contrib.console import confirm


def production():
    env.app_name = 'unisamenu'
    env.remote_name = 'heroku'


def staging():
    env.app_name = 'powerful-ocean-2885'
    env.remote_name = 'staging'


def test():
    with settings(warn_only=True):
        result = local('python manage.py test', capture=False)
    if result.failed and not confirm('Tests failed. Continue anyway?'):
        abort('Aborting at user request.')


def shell():
    require('app_name', provided_by=[staging, production])

    local('heroku run python manage.py shell --app %(app_name)s' % env)


def upload():
    require('app_name', provided_by=[staging, production])
    require('remote_name', provided_by=[staging, production])

    # prepare_deployment()

    local('git push %(remote_name)s master' % env)
    local('heroku run --app %(app_name)s python manage.py syncdb' % env)
    local('heroku run --app %(app_name)s python manage.py migrate' % env)
    local('heroku run --app %(app_name)s python manage.py collectstatic --noinput' % env)

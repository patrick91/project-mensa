(function($, window, document, undefined) {
    var $body, $saveButton, $message;

    orientationListener = function(evt) {
        var overThreshold = Math.abs(evt.gamma) > 4 || Math.abs(evt.beta) > 4;
        var gamma = overThreshold ? evt.gamma : 0;
        var beta = overThreshold ? evt.beta : 0;

        document.body.style.webkitTransform = 'rotateY(' + (-2.0 * gamma) + 'deg)';
    };

    showCacheStatus = function(n) {
        statusMessages = ["Uncached","Idle","Checking","Downloading","Update Ready","Obsolete"];
        return statusMessages[n];
    };

    saveCourses = function(e) {
        e.preventDefault();
        courses = [];

        var $courses = $('.course');

        $courses.each(function() {
            var $$ = $(this);

            courses.push({
                pk: $$.data('pk'),
                name: $$.find('.name').text(),
                description: $$.find('.description').text()
            });
        });

        $.ajax({
            type: 'POST',
            url: $saveButton.data('url'),
            data: {
                courses: courses,
                csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val()
            },
            dataType: 'json',
            beforeSend: function() {
                $message.text('Updating...');
            },
            success: function() {
                $message.text('Updated');
            }
        });
    };

    setEditableUI = function() {
        $message = $('#message');
        $saveButton.click(saveCourses);
    };


    $(function() {
        $body = $('body');
        $saveButton = $('#save');

        if ($saveButton) {
            setEditableUI();
        }

        if ($body.hasClass('error500')) {
            window.addEventListener('deviceorientation', orientationListener, false);
        }

        var $form = $('footer .newsletter');
        $form.delegate('input', 'focus', function() {
            $form.addClass('active');
        });
    });
})(jQuery, window, document);

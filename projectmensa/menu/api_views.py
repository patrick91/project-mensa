from datetime import date

from django.http import Http404

from rest_framework import status
from rest_framework import generics
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.mixins import CreateModelMixin

from .models import MenuOfTheDay, Device
from .serializers import MenuSerializer


@api_view(['GET'])
def api_root(request, format=None):
    """
    The entry endpoint of our API.
    """
    return Response({
        'menus': reverse('menu-list', request=request),
        'devices': reverse('devices-list', request=request),
    })


class MenuList(generics.ListAPIView):
    model = MenuOfTheDay
    serializer_class = MenuSerializer
    paginate_by = 10
    paginate_by_param = 'count'


class MenuDetail(generics.RetrieveAPIView):
    model = MenuOfTheDay
    serializer_class = MenuSerializer


class TodayMenu(generics.RetrieveAPIView):
    model = MenuOfTheDay
    serializer_class = MenuSerializer

    def get_object(self, queryset=None):
        try:
            return MenuOfTheDay.objects.get(day=date.today())
        except MenuOfTheDay.DoesNotExist:
            raise Http404()


class CreateDevice(generics.CreateAPIView, CreateModelMixin):
    model = Device

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try:
            model = Device.objects.get(key=request.DATA.get('key'))
            serializer = self.get_serializer(model)
            headers = self.get_success_headers(serializer.data)

            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)
        except Device.DoesNotExist:
            return super(CreateDevice, self).create(request, *args, **kwargs)

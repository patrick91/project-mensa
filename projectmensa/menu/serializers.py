from rest_framework import serializers

from .models import MenuOfTheDay


class MenuSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MenuOfTheDay
        depth = 1

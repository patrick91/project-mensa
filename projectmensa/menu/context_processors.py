from .forms import MailingListForm


def forms(request):
    form = MailingListForm()

    return {
        'mailing_list_form': form
    }

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from mailchimp.utils import get_connection
from mailchimp.chimpy.chimpy import ChimpyException


class MailingListForm(forms.Form):
    first_name = forms.CharField(label=_('Nome'), required=False)
    last_name = forms.CharField(label=_('Cognome'), required=False)
    email = forms.EmailField(label=_('Email'))

    def save(self, request=None):
        ml = get_connection().get_list_by_id(settings.MAILCHIMP_LIST_ID)

        try:
            ml.subscribe(self.cleaned_data['email'], {
                'EMAIL': self.cleaned_data['email'],
                'FNAME': self.cleaned_data['first_name'],
                'LNAME': self.cleaned_data['last_name'],
            })
        except ChimpyException:
            pass

        if request:
            request.session['subscribed'] = True

from django.contrib.syndication.views import Feed

from .models import MenuOfTheDay


class LatestMenusFeed(Feed):
    title = "Unisa Menu"
    link = "/"

    def items(self):
        return MenuOfTheDay.published_objects.order_by('-day')[:10]

    def item_title(self, item):
        return 'Menu del giorno %s' % item.day

    def item_link(self, item):
        return '/'

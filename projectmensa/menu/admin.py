from django.contrib import admin
from django.forms import ModelForm, ModelMultipleChoiceField

from models import *

from pdf_utils import pdf_to_courses


class CourseAdmin(admin.ModelAdmin):
    ordering = ['name']


class ProductAdminForm(ModelForm):
    courses = ModelMultipleChoiceField(queryset=Course.objects.order_by('name'))

    class Meta:
        model = MenuOfTheDay


class MenuAdmin(admin.ModelAdmin):
    ordering = ['-day']
    form = ProductAdminForm

    def save_model(self, request, obj, form, change):
        """
        L'admin di Django ha un bug con il salvataggio dei m2m
        http://goo.gl/MEUgt
        """

        super(MenuAdmin, self).save_model(request, obj, form, change)

        if obj.pdf == form.cleaned_data['pdf']:
            return

        courses = pdf_to_courses(obj.pdf)
        form.cleaned_data['courses'] = list(form.cleaned_data['courses'])

        for course in courses:
            if not course['name']:
                continue

            try:
                c = Course.objects.get(name=course['name'])
            except Course.DoesNotExist:
                c = Course(name=course['name'], description=course['description'],
                           type=course['type'])
                c.save()

            form.cleaned_data['courses'].append(c)

admin.site.register(Course, CourseAdmin)
admin.site.register(TypeToken)
admin.site.register(MenuOfTheDay, MenuAdmin)
admin.site.register(Device)

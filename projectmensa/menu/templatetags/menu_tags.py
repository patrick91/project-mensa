import re

from django import template

from ..models import *

register = template.Library()


class GetCourseTypeNode(template.Node):
    """
    Works only in regrouped dicts
    """
    def __init__(self, type_source, var_name):
        self.type_source = type_source
        self.var_name = var_name

    def render(self, context):
        type_source = context[self.type_source]['grouper']

        t = ''

        for choice in COURSE_TYPES:
            if choice[0] == type_source:
                t = choice[1]

        context[self.var_name] = t

        return ''


@register.tag()
def get_course_type(parser, token):
    try:
        # Splitting by None == splitting by spaces.
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires arguments" % token.contents.split()[0])
    m = re.search(r'(.*?) as (\w+)', arg)

    if not m:
        raise template.TemplateSyntaxError("%r tag had invalid arguments" % tag_name)

    type_source, var_name = m.groups()

    return GetCourseTypeNode(type_source, var_name)

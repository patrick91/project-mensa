# -*- coding: utf-8 -*-
import re
import cld

from django.core.mail import mail_managers

from pdfminer.pdfinterp import PDFResourceManager, process_pdf
from pdfminer.converter import PDFConverter
from pdfminer.layout import *
from pdfminer.pdfparser import PDFSyntaxError

from .models import TypeToken


class PDFParsingError(Exception):
    pass


class BracketsParsingException(Exception):
    pass


def is_italian(text):
    text = text.encode('utf-8')
    return cld.detect(text)[1] == 'it'


def normalize(text):
    # TODO
    return text


class MyTextConverter(PDFConverter):
    MIN_Y_DIFFERENCE = 2

    def __init__(self, rsrcmgr):
        self.out = []
        self._current = u""
        self._last_y = None

        PDFConverter.__init__(self, rsrcmgr, outfp=None, pageno=1)

    def write_text(self, text, y):  # TODO: controllare anche posizione x
        "Raggruppo le linee in base alla posizione y"
        if self._last_y is None:
            self._last_y = y

        if (self._last_y - y) > self.MIN_Y_DIFFERENCE:
            self._last_y = y

            if self._current.strip() != "":
                c = re.sub("\s\s+", " ", self._current.strip())

                self.out.append(c)

            self._current = u""

        self._current += text

    def receive_layout(self, ltpage):
        def render(item):
            if isinstance(item, LTContainer):
                for child in item:
                    render(child)
            elif isinstance(item, LTText):
                self.write_text(item.get_text(), item.y1)

        render(ltpage)

    def render_image(self, name, stream):
        pass

    def paint_path(self, gstate, stroke, fill, evenodd, path):
        pass


def pdf_to_text(file):
    rsrcmgr = PDFResourceManager()
    device = MyTextConverter(rsrcmgr)

    process_pdf(rsrcmgr, device, file)
    out = device.out

    device.close()

    return out


def any_in(needles, haystack):
    for needle in needles:
        if needle in haystack:
            return True

    return False


def get_type(course):
    """
    Ottiene il tipo del piatto in base ad un semplice controllo su
    alcune parole chiavi.
    Prima di tutto il nome del piatto viene trattato, convertendo caratteri
    diversi da a-z in spazi, poi viene spezzato in una lista.
    """
    course = re.sub("[^a-z]", ' ', course.lower())
    infos = [x for x in course.split()]

    if get_type._tokens == None:
        get_type._tokens = TypeToken.objects.filter(type__isnull=False)

    for token in get_type._tokens:
        if token.regex:
            if re.search(token.token, course):
                return token.type
        else:
            if token.token in infos:
                return token.type

    return 3
get_type._tokens = None


def convert_to_ascii(line):
    punctuation = {
        u'\u2018': "'",
        u'\u2019': "'",
        u'\u2013': "-",
    }

    line = line

    for src, dest in punctuation.iteritems():
        line = line.replace(src, dest)

    return line


def remove_brackets(word):
    if word.startswith('('):
        word += word[1:]

    if word.endswith(')'):
        word = word[:-1]

    return word


def pdf_to_courses(file, fail_silently=False):
    try:
        lines = [x.strip() for x in map(convert_to_ascii, pdf_to_text(file))]
    except PDFSyntaxError:
        raise PDFParsingError()

    useless_tokens = []

    for token in TypeToken.objects.filter(type__isnull=True):
        useless_tokens.append(token)

    def useless(line):
        for token in useless_tokens:
            if token.regex:
                if re.search(token.token, line):
                    return True
            else:
                if token.token in line.lower():
                    return True

        return False

    out = []

    i = 0
    complete = False

    while not complete:
        if i == len(lines):  # TODO: add in while condition
            complete = True
            continue

        second_description = ''
        current = lines[i]

        course = {
            'name': current,
            'description': ''
        }

        # Evito le cose inutili come la data data inserita all'inizio e i
        # consigli alimentari

        if useless(current):
            i += 1
            continue

        # I cestini di solito si trovano come ultima voce, quindi prendo tutto ciò
        # che rimane.
        # Dal 5 dicembre 2011 hanno aggunto anche la descrizione in inglese
        # messa come riga successiva quindi la salto e prendo come descrizione ciò
        # che segue la parola cestino (o cestini)

        if current.lower().startswith('cestin'):
            course['name'] = 'Cestini'
            course['description'] = current[7:].replace(':', '').strip('- ')
            complete = True
        else:
            try:
                next = lines[i + 1]

                if next.startswith('('):
                    course['description'] = next[1:]

                    while not next.endswith(')'):
                        i += 1

                        if (i + 1) < len(lines):
                            next = lines[i + 1]
                            course['description'] += " " + next
                        else:
                            raise BracketsParsingException()

                    course['description'] = course['description'][:-1]

                    i += 1

                next = lines[i + 1]

                # salto le cose in inglese (cfr. 05/12/2011)
                if next.startswith('('):
                    second_description = next[1:]

                    while not next.endswith(')'):
                        i += 1

                        if (i + 1) < len(lines):
                            next = lines[i + 1]
                            second_description += " " + next
                        else:
                            raise BracketsParsingException()

                    second_description = second_description[:-1]

                    i += 1

            except IndexError:
                complete = True
            except BracketsParsingException:
                if not fail_silently:
                    body = u"Si è verificato un problema con il menu di oggi.\n"
                    body += u"Piatto problematico: %s\n" % course['name'].decode('utf-8', 'replace')
                    body += "\n--\n"
                    body += u"Unisa Menu"

                    mail_managers(u'[Unisa Menu] Problema Parentesi', body)

                complete = True
                continue

        course['type'] = get_type(course['name'])

        # Controllo anche gli spazi in modo da evitare nomi composti
        if u'- ' in course['name'] or u' -' in course['name']:
            name, description = [x.strip() for x in course['name'].split('-')]
            course['name'] = name
            #course['description'] = remove_brackets(description)

        # Sembra che quando ci sia uno / ci sono due pietanze
        # TODO: bisogna verificare se capita sempre
        if '/' in course['name']:
            name, name2 = [x.strip() for x in course['name'].split('/')][0:2]
            course['name'] = name
            course['description'] = ''
            course['alternative_description'] = ''

            out.append(course)

            course['name'] = name2.capitalize()

        course['description'] = normalize(course['description'])
        second_description = normalize(second_description)

        if course['description'] and not is_italian(course['description']):
            if second_description:
                course['description'], second_description = second_description, course['description']
            else:
                course['description'], second_description = '', course['description']

        course['alternative_description'] = second_description

        out.append(course)

        i += 1

    return out

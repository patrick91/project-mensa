from django.conf.urls.defaults import *

from rest_framework.urlpatterns import format_suffix_patterns

from .api_views import *
from .feeds import LatestMenusFeed

urlpatterns = patterns('projectmensa.menu.views',
    url(r'^$', 'home'),

    url(r'(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/', 'menu_of_the_day'),
    url(r'^cache.manifest$', 'cache_manifest', name='cache-manifest'),
    url(r'^save_courses/$', 'save_courses', name='save-courses'),
    url(r'^subscribe/$', 'mailing_list_subscribe', name='mailing-list-subscribe'),

    url(r'^feed/$', LatestMenusFeed())
)

urlpatterns += patterns('projectmensa.menu.api_views',
    url(r'^api/$', 'api_root'),
    url(r'^api/menus/$', MenuList.as_view(), name='menu-list'),
    url(r'^api/menus/today/$', TodayMenu.as_view(), name='menuoftheday-today'),
    url(r'^api/menus/(?P<pk>\d+)/$', MenuDetail.as_view(), name='menuoftheday-detail'),
    url(r'^api/devices/$', CreateDevice.as_view(), name='devices-list'),
)

# Format suffixes
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])

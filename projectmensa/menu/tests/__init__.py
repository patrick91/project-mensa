import os

import cld

from django.test import TestCase

from ..pdf_utils import pdf_to_courses

FILES = {
    'menu_del_giorno_15_dicembre_2011_img.pdf': [
    {
        'description': "sale, olio extra vergine d'oliva, cipolle, carne di vitello e maiale, noce moscata, vino, prezzemolo, basilico, polpa di pomodoro e burro",
        'alternative_description': u'pasta tossed in pesto sauce made with fresh leaves of basil, peanuts, garlic, cappers, extra virgin olive oil and a touch of double cream',
        'name': 'Pasta alla Genovese',
        'type': 0
    }, {
        'description': "sale, olio extra vergine d'oliva, aglio, sedano, estratto brodo, purea di patate e polpa di pomodoro",
        'alternative_description': u'soup with lentils, extra virgin olive oil, onions, fresh parsley, tomato sauce and finished with small pasta tubes',
        'name': 'Pasta e lenticchie',
        'type': 0
    }, {
        'description': '',
        'name': 'Pasta in bianco o al pomodoro',
        'type': 0
    }, {
        'description': '',
        'alternative_description': '',
        'name': 'Riso in bianco',
        'type': 0
    }, {
        'description': 'carne di suino, carne di tacchino, pollo, peperone rosso, rosmarino, sale, vino bianco, pepe',
        'alternative_description': u'meat of pork, turkey and chicken, red peppers, rosemary, salt, peppercorn and white wine',
        'name': 'Spiedini al forno',
        'type': 1
    }, {
        'description': "Polpa di pomodoro, cipolle, olio d'oliva, patate, olive nere, prezzemolo, sale e pepe",
        'alternative_description': u'Fresh filets of plaice cooked in tomato sauce, onion, olive oil, potatoes, black olives, parsley, salt and pepper',
        'name': 'Filetti di platessa al guazzetto',
        'type': 1
    }, {
        'description': '',
        'alternative_description': '',
        'name': 'Mozzarella di bufala',
        'type': 1
    }, {
        'description': '',
        'alternative_description': '',
        'name': 'Formaggi misti',
        'type': 1
    }, {
        'description': '',
        'alternative_description': '',
        'name': 'Piatti freddi',
        'type': 1
    }, {
        'name': 'Verdure di stagione',
        'alternative_description': u'boiled seasonal mix vegetables',
        'description': '',
        'type': 2
    }, {
        'description': '',
        'alternative_description': '',
        'name': 'Insalata mista',
        'type': 2
    }, {
        'description': '',
        'alternative_description': '',
        'name': 'Frutta di stagione',
        'type': 3
    }, {
        'description': '',
        'alternative_description': 'Asiago- Coppa',
        'name': 'Cestini',
        'type': 3
    }],

    'menu_del_giorno_27_marzo_2012_img.pdf': [
        {
            'type': 0,
            'name': u'Pasta al rag\xf9 napoletano',
            'alternative_description': u'pasta tossed with a sauce made of onions, extra virgin olive oil, cubes of beef and tomato sauce',
            'description': u'sale, olio extra vergine d\'oliva, pomodoro, cipolla, basilico, vino e carne di vitello'
        }, {
            'type': 2,
            'name': u'Passato di verdure',
            'alternative_description': u'mix vegetable soup made with extra virgin olive oil, tomato, onions, fresh mixed vegetables all blended together',
            'description': u'verdure miste, olio d\'oliva, pomodoro, sale, cipolla'
        }, {
            'type': 0,
            'name': u'Pasta in bianco o al pomodoro',
            'alternative_description': '',
            'description': ''
        }, {
            'type': 0,
            'name': u'Riso in bianco',
            'alternative_description': '',
            'description': ''
        }, {
            'type': 1,
            'name': u'Seppie con giardiniera',
            'alternative_description': u'cuttlefish oven steamed then mixed with vegetables, extra virgin olive oil, fresh parsley, salt and pepper',
            'description': u'sale, olio extra vergine d\'oliva'
        }, {
            'type': 3,
            'name': u'Brasato di manzo',
            'alternative_description': u'braised beef with red wine, onions, celery, carrots, bay leaf, extra virgin olive oil, salt and pepper',
            'description': u'vino rosso, cipolle, carote, sedano, alloro, olio extra vergine d\'oliva, sale e pepe'
        }, {
            'type': 1,
            'name': u'Mozzarella di bufala',
            'description': '',
            'alternative_description': '',
        }, {
            'type': 1,
            'name': u'Formaggi misti',
            'description': '',
            'alternative_description': '',
        }, {
            'type': 1,
            'name': u'Piatti freddi',
            'description': '',
            'alternative_description': '',
        }, {
            'type': 2,
            'name': u'Insalata mista',
            'description': '',
            'alternative_description': '',
        }, {
            'type': 2,
            'name': u'Piselli al guanciale',
            'description': u'sale, olio extra vergine d\'oliva, pancetta, cipolla e burro',
            'alternative_description': u'peas with extra virgin olive oil, butter, onions and bacon',
        }, {
            'type': 3,
            'name': u'Frutta di stagione',
            'description': '',
            'alternative_description': '',
        }, {
            'type': 3,
            'name': 'Cestini',
            'description': u'prosciutto cotto - emmental',
            'alternative_description': '',
        }
    ],
}


LANGUAGE_SAMPLES = {
    'it': (
        "sale, olio extra vergine d'oliva, pomodoro, cipolla, basilico, vino e carne di vitello",
        "verdure miste, olio d'oliva, pomodoro, sale, cipolla",
        "vino rosso, cipolle, carote, sedano, alloro, olio extra vergine d'oliva, sale e pepe",
        "Polpa di pomodoro, cipolle, olio d'oliva, patate, olive nere, prezzemolo, sale e pepe",
    ),
    'en': (
        "Fresh filets of plaice cooked in tomato sauce, onion, olive oil, potatoes, black olives, parsley, salt and pepper",
        "pasta tossed with a sauce made of onions, extra virgin olive oil, cubes of beef and tomato sauce",
        "braised beef with red wine, onions, celery, carrots, bay leaf, extra virgin olive oil, salt and pepper",
        "mix vegetable soup made with extra virgin olive oil, tomato, onions, fresh mixed vegetables all blended together",
        "Fresh filets of plaice cooked in tomato sauce, onion, olive oil, potatoes, black olives, parsley, salt and pepper",
    ),
}


def get_file_path(f):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files', f)


class MenuTest(TestCase):
    fixtures = ['typetokens', ]

    def test_is_english(self):
        for text in LANGUAGE_SAMPLES['it']:
            self.assertTrue(cld.detect(text)[1] == 'it')

        for text in LANGUAGE_SAMPLES['en']:
            self.assertTrue(cld.detect(text)[1] == 'en')

    def test_pdf_parsing(self):
        for f, c in FILES.iteritems():
            fi = open(get_file_path(f), 'rb')
            courses = pdf_to_courses(fi)
            fi.close()

            self.assertEqual(len(courses), len(c))

            for course in courses:
                self.assertIn(course, c)

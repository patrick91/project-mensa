from gcm import GCM

from django.conf import settings

from rest_framework.renderers import JSONRenderer


def send_gcm_notification(title, menu):
    from .models import Device
    from .serializers import MenuSerializer

    ids = [str(x) for x in Device.objects.all().values_list('key', flat=True)]

    if len(ids) is 0:
        return

    gcm = GCM(settings.GCM_API_KEY)
    data = {
        'title': title,
        'menu': JSONRenderer().render(MenuSerializer(menu).data)
    }

    gcm.json_request(
        registration_ids=ids, data=data,
        collapse_key='menu', delay_while_idle=True, time_to_live=3600
    )

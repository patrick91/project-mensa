from django.db.models import Manager


class MenuManager(Manager):
    def get_query_set(self):
        return super(MenuManager, self).get_query_set().filter(published=True)

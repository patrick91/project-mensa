# -*- coding: utf-8 -*-
import nltk

english_vocab = set(w.lower() for w in nltk.corpus.words.words())
tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')


def is_english(text):
    """
    Funzione molto semplice per determinare se un testo è in inglese.
    """
    tokens = tokenizer.tokenize(text)

    text_vocab = set(w.lower() for w in tokens if w.lower().isalpha())
    unusual = text_vocab.difference(english_vocab)

    # print len(tokens), len(unusual), len(unusual) * 1.0 / len(tokens)
    # print unusual
    # print

    if float(len(unusual)) / len(tokens) < 0.3:
        return True
    return False

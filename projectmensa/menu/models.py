from datetime import date

from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from .managers import *
from .notifications import send_gcm_notification

COURSE_TYPES = (
        (0, _('Primo')),
        (1, _('Secondo')),
        (2, _('Contorno')),
        (3, _('Altro')),
    )


def get_menu_path(instance, filename):
    return '%(year)s/%(month)s/%(day)s.pdf' % {
        'year': instance.day.year,
        'month': str(instance.day.month).zfill(2),
        'day': str(instance.day.day).zfill(2),
    }


class TypeToken(models.Model):
    description = models.CharField(max_length=100, null=True, blank=True)
    token = models.CharField(max_length=100, unique=True)
    type = models.IntegerField(choices=COURSE_TYPES, null=True, blank=True)
    regex = models.BooleanField(default=False)

    class Meta(object):
        verbose_name = _('Type Token')
        verbose_name_plural = _('Type Tokens')

    def __unicode__(self):
        t = ''
        if self.type:
            t = self.get_type_display().lower()

        return '%(type)s - %(token)s' % {
            'token': self.token,
            'type': t,
        }


class Course(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(blank=True, null=True)
    alternative_description = models.TextField(blank=True, null=True)
    type = models.IntegerField(choices=COURSE_TYPES)

    class Meta(object):
        verbose_name = _('Piatto')
        verbose_name_plural = _('Piatti')

    def __unicode__(self):
        return self.name


class MenuOfTheDay(models.Model):
    day = models.DateField(unique=True, default=date.today())
    pdf = models.FileField(upload_to=get_menu_path)
    courses = models.ManyToManyField(Course, null=True, blank=True)
    published = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True)
    downloaded_from = models.CharField(max_length=255, blank=True, null=True)

    objects = models.Manager()
    published_objects = MenuManager()

    class Meta(object):
        verbose_name = _(u'Menu del giorno')
        verbose_name_plural = _(u'Menu del giorno')

    def __unicode__(self):
        return 'Menu for %s' % self.day


class Device(models.Model):
    key = models.CharField(max_length=200, unique=True)

    class Meta(object):
        verbose_name = _('Dispositivo')
        verbose_name_plural = _('Dispositivi')

    def __unicode__(self):
        return self.key


def post_menu_save(sender, **kwargs):
    menu = kwargs['instance']

    if menu.published:
        send_gcm_notification('New menu', menu)

        print 'notification sent'

post_save.connect(post_menu_save, sender=MenuOfTheDay)

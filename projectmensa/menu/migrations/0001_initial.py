# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Course'
        db.create_table('menu_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('menu', ['Course'])

        # Adding model 'MenuOfTheDay'
        db.create_table('menu_menuoftheday', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('day', self.gf('django.db.models.fields.DateField')(default=datetime.date(2011, 12, 14), unique=True)),
            ('pdf', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('menu', ['MenuOfTheDay'])

        # Adding M2M table for field courses on 'MenuOfTheDay'
        db.create_table('menu_menuoftheday_courses', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuoftheday', models.ForeignKey(orm['menu.menuoftheday'], null=False)),
            ('course', models.ForeignKey(orm['menu.course'], null=False))
        ))
        db.create_unique('menu_menuoftheday_courses', ['menuoftheday_id', 'course_id'])


    def backwards(self, orm):
        
        # Deleting model 'Course'
        db.delete_table('menu_course')

        # Deleting model 'MenuOfTheDay'
        db.delete_table('menu_menuoftheday')

        # Removing M2M table for field courses on 'MenuOfTheDay'
        db.delete_table('menu_menuoftheday_courses')


    models = {
        'menu.course': {
            'Meta': {'object_name': 'Course'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'type': ('django.db.models.fields.IntegerField', [], {})
        },
        'menu.menuoftheday': {
            'Meta': {'object_name': 'MenuOfTheDay'},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['menu.Course']", 'null': 'True', 'blank': 'True'}),
            'day': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2011, 12, 14)', 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pdf': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['menu']

{% load menu_tags %}
{% regroup courses|dictsort:"type" by type as types %}
Menu del giorno: **{{ menu.day }}**

{% for type in types %}
{% get_course_type type as course_type %}
{{ course_type }}
=================

{% for course in type.list %}
{{ course }}
------------
{{ course.description }}

{% endfor %}{% endfor %}

from django.conf import settings
from django.contrib import admin
from django.conf.urls.defaults import *
from django.views.decorators.cache import never_cache
from django.views.generic.simple import direct_to_template

admin.autodiscover()

urlpatterns = patterns('',
    (r'^', include('projectmensa.menu.urls')),

    (r'^admin/', include(admin.site.urls)),

    ('^500$', direct_to_template, {'template': '500.html'}),
    ('^404$', direct_to_template, {'template': '404.html'}),

    ('^robots.txt$', never_cache(direct_to_template), {'template': 'other/robots.txt'}),
)

if not settings.DEBUG:
    urlpatterns += patterns('',
        ('^googlef537e1aad82499f8.html$', direct_to_template, {
            'template': 'other/googlef537e1aad82499f8.html',
        }),
    )

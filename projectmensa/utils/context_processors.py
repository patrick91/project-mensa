from django.conf import settings


def debug_processor(request):
    return {
        'DEBUG': settings.DEBUG
    }

import os
import django
import django.conf.global_settings as DEFAULT_SETTINGS

from postgresify import postgresify

DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__file__))
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

path = lambda *args: os.path.join(SITE_ROOT, *args)

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Patrick Guido Arminio', 'patrick.arminio@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = postgresify()

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Rome'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'it-it'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = path('media')
STATIC_ROOT = path('static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = 'http://unisamenu.s3.amazonaws.com/'
MEDIA_URL = STATIC_URL + 'media/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/media/'

# Additional locations of static files
STATICFILES_DIRS = (
    path('global_static'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '(%e-v$e@ecj_wdx(ib_tn9eqkk2onjn*#lsb^3(gj-_u(b&2!2'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'projectmensa.urls'

TEMPLATE_DIRS = (
    path('templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',

    'projectmensa.menu',
    'projectmensa.menu.templatetags',

    'south',
    'chunks',
    'mailchimp',
    'storages',
    'rest_framework',
)

TEST_EXCLUDE = (
    'django',
    'mailchimp',
)

TEST_RUNNER = 'projectmensa.utils.AdvancedTestSuiteRunner'
TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
    'projectmensa.utils.context_processors.debug_processor',
    'django.core.context_processors.request',
    'projectmensa.menu.context_processors.forms',
)

EMAIL_BACKEND = 'django_ses.SESBackend'
SERVER_EMAIL = DEFAULT_FROM_EMAIL = 'patrick.arminio@gmail.com'

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

MAILCHIMP_API_KEY = os.environ.get('MAILCHIMP_API_KEY', '')
MAILCHIMP_LIST_ID = os.environ.get('MAILCHIMP_LIST_ID', '')

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID', '')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', '')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME', 'unisamenu')
AWS_QUERYSTRING_AUTH = False

MAILCHIMP_WEBHOOK_KEY = os.environ.get('MAILCHIMP_WEBHOOK_KEY', 'unisamenu1')

PUSHOVER_TOKEN = os.environ.get('PUSHOVER_TOKEN', '')
PUSHOVER_USER = os.environ.get('PUSHOVER_USER', '')
GCM_API_KEY = os.environ.get('GCM_API_KEY', '')

try:
    from local_settings import *
except ImportError:
    pass

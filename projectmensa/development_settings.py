import os

from settings import *
from bundle_config import config

path = lambda *args: os.path.join(config['core']['data_directory'], *args)

DEBUG = True
MEDIA_ROOT = path('media')

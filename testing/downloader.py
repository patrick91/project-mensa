import os
import calendar
import datetime
import locale
import urlparse
import requests

from collections import OrderedDict

BASE = 'http://www.adisu.sa.it/fileadmin/user_upload/menu/'
OUT = 'out/'

calendar.setfirstweekday(calendar.MONDAY)

locale.setlocale(locale.LC_ALL, "it_IT.UTF-8")


def download_menu(url, path):
    remote_file = requests.get(url)

    if remote_file.status_code != 200:
        print remote_file.status_code, 'error with', url
        return

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    local_file = open(path, 'w')
    local_file.write(remote_file.content)

    local_file.close()

months_choices = []

m = OrderedDict()

year = 2011

for month in range(0, 12):
    days = calendar.mdays[month + 1]
    month_name = datetime.date(year, month + 1, 1).strftime('%B').lower()
    m[month_name] = []

    for day in range(1, days + 1):
        #solo da lunedi al venerdi
        if calendar.weekday(year, month + 1, day) <= 4:
            m[month_name].append(day)

for month in m.keys():
    for day in m[month]:
        filename = 'menu_del_giorno_%d_%s_2011_img.pdf' % (day, month)

        url = urlparse.urljoin(BASE, filename)
        path = os.path.join(OUT, month, str(day).zfill(2) + '.pdf')
        download_menu(url, path)
